#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <string>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    void loadFromFile();
    void calChain();
    void saveToFile();

private:
    Ui::MainWindow *ui;
    static int getWords(const QString &qString, char **words);
};
#endif // MAINWINDOW_H
